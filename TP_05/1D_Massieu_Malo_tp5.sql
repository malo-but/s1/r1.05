-- Q5

/*
EnseignantInfo(noEns(1), nomENS, prenom1, prenom2)
EtudiantInfo(noEtu(1), nomEtu, prenom, promotion, groupe)
*/

-- Q6

-- EtudiantInfo[prenom]

SELECT DISTINCT prenom FROM EtudiantInfo;

/*
142 tuples
Plusieurs étudiants ont le même prénom et aucune vérification n'est effectuée sur
la syntaxe des prénoms (ex: 'Clement' et 'CLeMENT' sont considérés comme différents)
*/

-- Q7

-- EtudiantInfo{prenom = 'Louis'}

SELECT * FROM EtudiantInfo WHERE prenom = 'Louis';

/*
NOETU	NOMETU	PRENOM	PROMOTION	GROUPE
4		BARRIAC	Louis	INFO1		D
*/

-- Q8

-- EtudiantInfo{prenom = 'Clement' OU prenom = 'Pierre'}

SELECT * FROM EtudiantInfo WHERE UPPER(prenom) = 'CLEMENT' OR UPPER(prenom) = 'PIERRE';

/*
NOETU	NOMETU				PRENOM	PROMOTION	GROUPE
9		BELLIER				Clement	INFO1		A
45		HOUSSIN--VONTHRON	Clement	INFO1		A
148		MAILLET				CLeMENT	INFO2		B
155		MONFORT				Clement	INFO2		B
*/

-- Q9

-- EtudiantInfo{prenom = 'Malo' ET promotion = 'INFO2'}

SELECT * FROM EtudiantInfo WHERE UPPER(prenom) = 'MALO' AND UPPER(promotion) = 'INFO2';

-- Aucun tuple correspondant

-- Q10

-- EnseignantInfo[prenom1] u EtudiantInfo[prenom]

SELECT DISTINCT prenom1 FROM EnseignantInfo
UNION
SELECT DISTINCT prenom FROM EtudiantInfo;

-- 155 tuples

-- Q11

-- EnseignantInfo[prenom1] n EtudiantInfo[prenom]

SELECT DISTINCT prenom1 FROM EnseignantInfo
INTERSECT
SELECT DISTINCT prenom FROM EtudiantInfo;

/*
PRENOM1
Anthony
Francois
Goulven
Matthieu
*/

-- Q12

-- EnseignantInfo[prenom1] \ EtudiantInfo[prenom]

SELECT DISTINCT prenom1 FROM EnseignantInfo
MINUS
SELECT DISTINCT prenom FROM EtudiantInfo;

-- 13 tuples 

-- Q13

-- EnseignantInfo{prenom2 = NULL}[nomEns]

SELECT DISTINCT nomENS FROM EnseignantInfo WHERE prenom2 IS NULL;

/*
NOMENS
LeMaitre
KAmP
LE Lain
Lesueur
*/

-- Q14

-- EtudiantInfo{nomEtu commence par 'A' ET nomEtu contient 'A'}[nomEtu, prenom]

SELECT DISTINCT nomEtu, prenom FROM EtudiantInfo WHERE UPPER(nomEtu) LIKE 'A%A%';

/*
NOMETU				PRENOM
AGBOGBO (BASSON)	Deborah
ANNASRI				Samy
*/

-- Q15

-- EtudiantInfo{nomEtu < prenom ET nomEtu contient 'A'}[noEtu, nomEtu]

SELECT DISTINCT noEtu, nomEtuFROM FROM EtudiantInfo WHERE UPPER(nomEtu) < UPPER(prenom) AND UPPER(nomEtu) LIKE '%A';

/*
NOETU	NOMETU
25		DA ROCHA
136		JOMAA
*/

-- Q16

SELECT *
FROM (
	SELECT DISTINCT UPPER(nomEtu)
	FROM EtudiantInfo
	ORDER BY UPPER(nomEtu);
)
WHERE ROWNUM <= 10;

/*
UPPER(NOMETU)
AGBOGBO (BASSON)
AIME
ANNASRI
ANTONIO
AUGER
BARRIAC
BASOL
BATARD
BEATRIX
BELBEOCH
*/
