/*
Film(code(1),titre, leRealisateur = @Personne[idPersonne], anneeSortie, duree)
Personne(idPersonne(1), nom, prenom, pays, dateNaissance)
Acteur((unActeur = @Personne[idPersonne], unFilm = @Film[code])(1), cachet)
*/

-- Q3

-- Film[titre]

SELECT DISTINCT UPPER(titre) 
FROM Film;

/*

- UPPER(TITRE)
CESAR
MINUIT A PARIS
LUCY
HUNGER GAMES : L EMBRASEMENT
BONS BAISERS DE RUSSIE

95 rows selected.

*/

-- Q4

-- Film{anneeSortie = 2019}[titre]

SELECT DISTINCT UPPER(titre) 
FROM Film 
WHERE anneeSortie = 2019;

/*

- UPPER(TITRE)
ONCE UPON A TIME ... IN HOLLYWOOD
ALITA : BATTLE ANGEL
AD ASTRA

3 rows selected.

*/

-- Q5

-- Film{anneeSortie >= 2017 AND anneeSortie <= 2019}[titre]

SELECT DISTINCT UPPER(titre) 
FROM Film 
WHERE anneeSortie BETWEEN 2017 AND 2019;

/*

- UPPER(TITRE)
PIRATES DES CARAIBES : LA VENGEANCE DE SALAZAR
MISSION IMPOSSIBLE : FALLOUT
ONCE UPON A TIME ... IN HOLLYWOOD
ALITA : BATTLE ANGEL
AD ASTRA
DUNKERQUE

6 rows selected.

*/

-- Q6

-- Personne{leRealisateur IN Film OR unActeur IN Acteur}[nom]

SELECT DISTINCT nom 
FROM Personne 
WHERE idPersonne IN (
	SELECT leRealisateur 
	FROM Film
	) OR idPersonne IN (
	SELECT unActeur 
	FROM Acteur
);

/*

- NOM
Volkswagen
Cruise
Blanchett
Johansson
Connery

109 rows selected.

*/

-- Q7

-- Film{leRealisateur IN Personne{pays = 'Japon'}}[titre]

SELECT DISTINCT UPPER(titre) 
FROM Film 
WHERE leRealisateur IN (
	SELECT idPersonne 
	FROM Personne 
	WHERE pays = 'Japon'
);

/*

- UPPER(TITRE)
LES SEPT SAMOURAIS
LA FORTERESSE CACHEE
DERSOU OUZALA

3 rows selected

*/

-- Q8

-- Personne{Film{anneeSortie >= 2014 AND anneeSortie <= 2020}[leRealisateur]}{pays = 'France'}[nom, prenom]

SELECT prenom, nom 
FROM Personne 
WHERE idPersonne IN (
	SELECT leRealisateur 
	FROM Film 
	WHERE anneeSortie BETWEEN 2014 AND 2020) AND pays = 'France';

/*

- PRENOM	NOM
Luc			Besson
Zem			Roschdy

2 rows selected

*/

-- Q9

-- Film{leRealisateur IN Personne{prenom = 'Luc' AND nom = 'Besson'}}[titre, anneeSortie]

SELECT UPPER(titre), anneeSortie 
FROM Film 
WHERE leRealisateur IN (
	SELECT idPersonne 
	FROM Personne 
	WHERE nom = 'Besson' AND prenom = 'Luc'
);

/*

- UPPER(TITRE)										ANNEESORTIE
LES AVENTURES EXTRAORDINAIRES D ADELE BLANC SEC		2010
LUCY												2014
LE GRAND BLEU										1988

3 rows selected

*/

-- Q10

-- Personne{leRealisateur NOT IN Film}[nom, prenom]

SELECT DISTINCT nom, prenom 
FROM Personne 
WHERE idPersonne NOT IN (
	SELECT leRealisateur 
	FROM Film
);

/*

- NOM		PRENOM
Hemsworth	Liam
RHAMES		Ving
James		Theo
Popplewell	Anna
PEGG		Simon

68 rows selected.

*/

-- Q11

-- Personne{unActeur IN Acteur{cachet < 10000}}[nom, prenom]

SELECT DISTINCT nom, prenom 
FROM Personne 
WHERE idPersonne IN (
	SELECT unActeur 
	FROM Acteur 
	WHERE cachet < 10000
);

/*

- NOM		PRENOM
KurosawA	Akira
Neige		Blanche
Marceau		Sophie
Moreno		Guy
Cantona		Jean

10 rows selected.

*/

-- Q12

SELECT DISTINCT UPPER(Film1.titre), UPPER(Film2.titre)
FROM Film Film1, Film Film2
WHERE Film1.anneeSortie = Film2.anneeSortie AND UPPER(Film1.titre) != UPPER(Film2.titre)

/*

- UPPER(FILM1.TITRE)			UPPER(FILM2.TITRE)
LES SEPT MERCENAIRES			LES SEPT BOULES DE CRISTAL
LE VENT SOUFFLERA DEUX FOIS		LE MOULIN ROUGE
LE VENT SOUFFLERA DEUX FOIS		LA ROUTE DE MAGGERSFONTEIN
LE MOULIN ROUGE					DERSOU OUZALA
LE VENT SOUFFLERA DEUX FOIS		DERSOU OUZALA

144 rows selected.

*/

-- Q13

SELECT DISTINCT nom, prenom 
FROM Personne 
WHERE idPersonne IN (
	SELECT unActeur 
	FROM Acteur 
	WHERE unFilm IN (
		SELECT code 
		FROM Film 
		WHERE titre = 'Hunger Games')
);

/*

- NOM		PRENOM
Hemsworth	Liam
Lawrence	Jennifer
Hutcherson	Joshua

3 rows selected.

*/

-- Q14

SELECT DISTINCT UPPER(titre) 
FROM Film 
WHERE code IN (
	SELECT unFilm 
	FROM Acteur 
	WHERE unActeur IN (
		SELECT idPersonne 
		FROM Personne 
		WHERE prenom = 'Tom' AND nom = 'Cruise')
);

/*

- UPPER(TITRE)
MISSION IMPOSSIBLE : FALLOUT
MISSION IMPOSSIBLE
OBLIVION
MISSION IMPOSSIBLE 3
MISSION IMPOSSIBLE : ROGUE NATION

8 rows selected.

*/

-- Q15

SELECT DISTINCT nom, prenom 
FROM Personne 
WHERE idPersonne IN (
	SELECT unActeur 
	FROM Acteur 
	WHERE unFilm IN (
		SELECT code 
		FROM Film 
		WHERE leRealisateur IN (
			SELECT idPersonne 
			FROM Personne 
			WHERE prenom = 'Woody' AND nom = 'Allen'))
);

/*

- NOM		PRENOM
Blanchett	Kate
Cottilard	Marion
Cruz		Penelope
Allen		Woody
Elmaleh		Gad

5 rows selected.

*/

-- Q16

SELECT DISTINCT UPPER(titre) 
FROM Film 
WHERE leRealisateur IN (
	SELECT unActeur 
	FROM Acteur
);

/*

- UPPER(TITRE)
MINUIT A PARIS
LUCY
BUCK
TO ROME WITH LOVE
LES SEPT SAMOURAIS

19 rows selected.

*/