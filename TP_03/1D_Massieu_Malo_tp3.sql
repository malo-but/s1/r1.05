-- ======================== Script de création de tables pour le TP3 (cf. TP2, exercice 1)
/* 
Enseignant( nomEns (1), prenomEns, adresse, statut )  
Cycle( num (1), enseignantResponsable = @Enseignant.nomEns (UQ)(NN) )
Cours( nomCours (1), volumeH, lEnseignant = @Enseignant.nomEns (NN), leCycle = @Cycle.num (NN) ) 
Requiert( [ cours = @Cours.nomCours, coursRequis = @Cours.nomCours ](1) )
*/

-- Q1

DROP TABLE Requiert ;
DROP TABLE Cours ;
DROP TABLE Cycle ; 
DROP TABLE Enseignant ;

CREATE TABLE Enseignant
	(
	nomEns VARCHAR2(20)
		CONSTRAINT pk_Enseignant PRIMARY KEY,	
	prenomEns VARCHAR2(20),
	adresse VARCHAR2(40),
	statut VARCHAR2(20)
	)
;

CREATE TABLE Cycle
	(
	num NUMBER
		CONSTRAINT pk_Cycle PRIMARY KEY,	
	enseignantResponsable VARCHAR2(20) 
		CONSTRAINT fk_Cycle_Enseignant REFERENCES Enseignant(nomEns)
		CONSTRAINT uq_enseignantResponsable UNIQUE
		CONSTRAINT nn_enseignantResponsable NOT NULL
	)
;

CREATE TABLE Cours
	(
	nomCours VARCHAR2(20)
		CONSTRAINT pk_Cours PRIMARY KEY,	
	volumeH NUMBER,
	lEnseignant VARCHAR2(20)
		CONSTRAINT fk_Cours_Enseignant REFERENCES Enseignant(nomEns)
		CONSTRAINT nn_lEnseignant NOT NULL,	
	leCycle NUMBER
		CONSTRAINT fk_Cours_Cycle REFERENCES Cycle(num)	
		CONSTRAINT nn_leCycle NOT NULL
	)
;

CREATE TABLE Requiert
	(
	cours VARCHAR2(20)
		CONSTRAINT fk_Requiert_Cours REFERENCES Cours(nomCours), 
	coursRequis VARCHAR2(20)
		CONSTRAINT fk_Requiert_CoursRequis REFERENCES Cours(nomCours), 
	CONSTRAINT pk_Requiert PRIMARY KEY (cours, coursRequis)
	)
;

-- Q2

-- Ajout des enseignants

INSERT INTO Enseignant(nomEns) VALUES('Kamp');

INSERT INTO Enseignant(nomEns, prenomEns) VALUES('Lesueur', 'François');

INSERT INTO Enseignant(nomEns, prenomEns, adresse)
	VALUES('Dupont', 'Charles', '12 rue Montaigne');

INSERT INTO Enseignant(nomEns, prenomEns, adresse, statut)
	VALUES('Gnanhouan', 'Marius', '25 Poul-Ranet', 'Vacataire');

INSERT INTO Enseignant(nomEns, prenomEns, adresse, statut)
	VALUES('Ridard', 'Anthony', 'Vannes', 'Titulaire');

-- Ajout des cycles

INSERT INTO Cycle(num, enseignantResponsable) VALUES(1, 'Kamp');

INSERT INTO Cycle(num, enseignantResponsable) VALUES(3, 'Gnanhouan');

INSERT INTO Cycle(num, enseignantResponsable) VALUES(6, 'Ridard');

-- Ajout des cours

INSERT INTO Cours(nomCours, volumeH, lEnseignant, leCycle)
	VALUES('Java', 30, 'Kamp', 1);

INSERT INTO Cours(nomCours, volumeH, lEnseignant, leCycle)
	VALUES('Anglais', 30, 'Gnanhouan', 3);

INSERT INTO Cours(nomCours, volumeH, lEnseignant, leCycle)
	VALUES('Maths', 30, 'Ridard', 6);

INSERT INTO Cours(nomCours, volumeH, lEnseignant, leCycle)
	VALUES('UNIX', 30, 'Kamp', 1);

INSERT INTO Cours(nomCours, volumeH, lEnseignant, leCycle)
	VALUES('Web', 30, 'Gnanhouan', 3);

-- Ajout des requis de cours

INSERT INTO Requiert(cours, coursRequis) VALUES('Java', 'Anglais');

INSERT INTO Requiert(cours, coursRequis) VALUES('Java', 'Maths');

-- Q3

DELETE FROM Requiert;
DELETE FROM Cours;
DELETE FROM Cycle;
DELETE FROM Enseignant;

-- Q4

-- a)
INSERT INTO Enseignant(nomEns) VALUES(NULL);

-- b)
INSERT INTO Enseignant(nomEns) VALUES('Kamp');
INSERT INTO Enseignant(nomEns) VALUES('Kamp');

-- c)
INSERT INTO Cycle(num, enseignantResponsable) VALUES(1, NULL);

INSERT INTO Cycle(num, enseignantResponsable) VALUES(1, 'Kamp');
INSERT INTO Cycle(num, enseignantResponsable) VALUES(1, 'Kamp');

-- Q5

-- a)
ALTER TABLE Cours ADD CONSTRAINT ck_volumeH CHECK (volumeH > 0);

-- b)
INSERT INTO Cours(nomCours, volumeH, lEnseignant, leCycle)
	VALUES('Java', -1, 'Kamp', 1);

-- c)
ALTER TABLE Cours DROP CONSTRAINT ck_volumeH;

-- Q6

ALTER TABLE Enseignant ADD dateNaissance DATE;

ALTER TABLE Enseignant DROP COLUMN dateNaissance;

-- Q7

UPDATE Enseignant SET adresse = 'Vannes' WHERE nomEns = 'Kamp';

DELETE FROM Requiert WHERE coursRequis = 'Anglais';

