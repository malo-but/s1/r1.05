-- Exercice 1

/*

Ouvrage ( idOuvrage (1), titre, unAuteur = @Auteur.idAuteur (NN), anneeAchat )
Auteur ( idAuteur (1), nom (NN), prenom, nationalite, anneeNaissance )
Client ( idClient (1), nomClient (NN), adresse )
Emprunt ( [unClient = @Client.idClient, unOuvrage = @Ouvrage.idOuvrage](1), dateEmprunt )

Contraintes textuelles :
	- anneeAchat ne doit pas dépasser pas 2021
	- les attributs idOuvrage, idAuteur, idClient, anneeAchat et anneeNaissance sont de type NUMBER
	- l’attribut dateEmprunt est de type DATE
	- les autres attributs sont de type VARCHAR2(.)

*/

-- Q2

/*

Création des tables :
	- Client
	- Auteur
	- Ouvrage
	- Emprunt

Destruction des tables :
	- Emprunt
	- Ouvrage
	- Auteur
	- Client
	
*/

DROP TABLE Emprunt ;
DROP TABLE Ouvrage ;
DROP TABLE Auteur ;
DROP TABLE Client ;

-- Q3

CREATE TABLE Client
	(
	idClient NUMBER(1) 
		CONSTRAINT pk_Client PRIMARY KEY,

	nomClient VARCHAR2(30) 
		CONSTRAINT nn_nomClient NOT NULL,

	adresse VARCHAR2(50)
	)
;

CREATE TABLE Auteur
	(
	idAuteur NUMBER 
		CONSTRAINT pk_Auteur PRIMARY KEY,

	nom VARCHAR2(30) 
		CONSTRAINT nn_nom NOT NULL,

	prenom VARCHAR2(30),

	nationalite VARCHAR2(30),

	anneeNaissance NUMBER(4)
	)
;

CREATE TABLE Ouvrage
	(
	idOuvrage NUMBER
		CONSTRAINT pk_Ouvrage PRIMARY KEY,

	titre VARCHAR2(30),

	unAuteur NUMBER 
		CONSTRAINT fk_Ouvrage_Auteur REFERENCES Auteur(idAuteur)
		CONSTRAINT nn_unAuteur NOT NULL,

	anneeAchat NUMBER(4)
		CONSTRAINT ck_anneeAchat CHECK (anneeAchat < 2022)
	)
;

CREATE TABLE Emprunt
	(
	unClient NUMBER 
		CONSTRAINT fk_Emprunt_Client REFERENCES Client(idClient),

	unOuvrage NUMBER 
		CONSTRAINT fk_Emprunt_Ouvrage REFERENCES Ouvrage(idOuvrage),

	dateEmprunt DATE,

	CONSTRAINT pk_Emprunt PRIMARY KEY (unClient, unOuvrage)
	)
;

-- Q4

ALTER TABLE Emprunt MODIFY dateEmprunt DATE
	CONSTRAINT nn_dateEmprunt NOT NULL;

-- Q5

INSERT INTO Client VALUES (1, 'Massieu', '28 rue de Normandie');
INSERT INTO Client VALUES (2, 'Rocabois', '7 route de la corniche');

INSERT INTO Auteur VALUES (1, 'Christie', 'Agatha', 'Britannique', 1990);
INSERT INTO Auteur VALUES (2, 'Tim', 'Burton', 'Américain', 1958);

INSERT INTO Ouvrage VALUES (1, 'Hercule Poirot', 1, 1926);
INSERT INTO Ouvrage VALUES (2, 'La Famille Addams', 2, 2019);

INSERT INTO Emprunt VALUES (1, 1, TO_DATE('09/12/2022', 'DD/MM/YYYY'));
INSERT INTO Emprunt VALUES (2, 2, TO_DATE('01/01/2016', 'DD/MM/YYYY'));

-- Q6

INSERT INTO Ouvrage VALUES (3, 'Le Livre de la jungle', 3, 2016);

DELETE FROM Auteur WHERE idAuteur = 2;

-- Q7

ALTER TABLE Emprunt ADD dateRetour DATE;

ALTER TABLE Emprunt ADD CONSTRAINT ck_dateRetour CHECK (dateRetour > dateEmprunt);

-- Q8

UPDATE Auteur SET anneeNaissance = 1890 WHERE idAuteur = 1;

SELECT * FROM Auteur;

-- Exercice 2

-- Q9

/*

Concession(idConc(1), nomConc, capital)

Constructeur(idConst(1), nomConst(NN))

Client(idClient(1), nomClient(2), prenomClient(2))

Voiture(immat(1), modele(NN), couleur, laConcession = @Concession.idConc,
		leConstructeur = @Constructeur.idConst(NN), leClient = @Client.idClient)

Travail([uneConc = @Concession.idConc, unConst = @Constructeur.idConst](1))

Assurance([unConst = @Constructeur.idConst, unClient = @Client.idClient](1), dateContrat)

*/

-- Q10

-- Destruction des tables pour ré-execution

DROP TABLE Assurance ;
DROP TABLE Travail ;
DROP TABLE Voiture ;
DROP TABLE Client ;
DROP TABLE Constructeur ;
DROP TABLE Concession ;

-- Création des tables

CREATE TABLE Concession
	(
	idConc NUMBER
		CONSTRAINT pk_Concession PRIMARY KEY,

	nomConc VARCHAR2(30),

	capital NUMBER
	)
;

CREATE TABLE Constructeur
	(
	idConst NUMBER
		CONSTRAINT pk_Constructeur PRIMARY KEY,

	nomConst VARCHAR2(30) 
		CONSTRAINT nn_nomConst NOT NULL
	)
;

CREATE TABLE Client
	(
	idClient NUMBER
		CONSTRAINT pk_Client PRIMARY KEY,

	nomClient VARCHAR2(30) 
		CONSTRAINT nn_nomClient NOT NULL,

	prenomClient VARCHAR2(30) 
		CONSTRAINT nn_prenomClient NOT NULL
	)
;

CREATE TABLE Voiture
	(
	immat NUMBER
		CONSTRAINT pk_Voiture PRIMARY KEY,

	modele VARCHAR2(30) 
		CONSTRAINT nn_modele NOT NULL,

	couleur VARCHAR2(30),

	laConcession NUMBER 
		CONSTRAINT fk_Voiture_Concession REFERENCES Concession(idConc),

	leConstructeur NUMBER 
		CONSTRAINT fk_Voiture_Constructeur REFERENCES Constructeur(idConst)
		CONSTRAINT nn_leConstructeur NOT NULL,

	leClient NUMBER 
		CONSTRAINT fk_Voiture_Client REFERENCES Client(idClient)
	)
;

CREATE TABLE Travail
	(
	uneConc NUMBER 
		CONSTRAINT fk_Travail_Concession REFERENCES Concession(idConc),

	unConst NUMBER 
		CONSTRAINT fk_Travail_Constructeur REFERENCES Constructeur(idConst),

	CONSTRAINT pk_Travail PRIMARY KEY (uneConc, unConst)
	)
;

CREATE TABLE Assurance
	(
	unConst NUMBER
		CONSTRAINT fk_Assurance_Constructeur REFERENCES Constructeur(idConst),

	unClient NUMBER
		CONSTRAINT fk_Assurance_Client REFERENCES Client(idClient),

	dateContrat DATE
	)
;

-- Q11

ALTER TABLE Assurance MODIFY dateContrat DATE
	CONSTRAINT nn_dateContrat NOT NULL;

-- Q12

ALTER TABLE Client ADD email VARCHAR2(30);

ALTER TABLE Client ADD CONSTRAINT ck_email CHECK (email LIKE '%@%');

-- Q13

INSERT INTO Concession VALUES (1, 'Renault', 1000000);

INSERT INTO Constructeur VALUES (1, 'Renault');

INSERT INTO Client VALUES (1, 'Dupont', 'Jean', 'dupont.jean@gmail.com');

INSERT INTO Voiture VALUES (1, 'Clio', 'Rouge', 1, 1, 1);

INSERT INTO Voiture VALUES (2, 'Twingo', 'Bleu', 1, 1, 1);

-- Q14

-- INSERT INTO

INSERT INTO Voiture VALUES (3, 'Clio', 'Rouge', 1, 1, 1);

INSERT INTO Voiture VALUES (4, 'Clio', 'Rouge', 1, 1, 1);

INSERT INTO Voiture VALUES (5, 'Clio', 'Rouge', 1, 1, 1);

-- DELETE FROM

DELETE FROM Voiture WHERE immat = 1;

-- ALTER TABLE

ALTER TABLE Client ADD concession NUMBER;

ALTER TABLE Client ADD CONSTRAINT fk_Client_Concession FOREIGN KEY (concession) REFERENCES Concession(idConc);

-- UPDATE

UPDATE Voiture SET leClient = 1 WHERE immat = 2;
